from configparser import ConfigParser
from binance.client import Client
from binance.websockets import BinanceSocketManager
from flask import Flask,jsonify
import pymongo
from pymongo import MongoClient
from flask_cors import CORS
from flask_apscheduler import APScheduler
import pytz
from datetime import datetime,timezone,timedelta
import time
app = Flask(__name__)
scheduler = APScheduler()
CORS(app)
configur = ConfigParser()
configur.read("configs/config.ini")
configur.sections()
tz = timezone(timedelta(hours=7))

client = Client(configur["BINANCE"]["ACTUAL_API_KEY"], configur["BINANCE"]["ACTUAL_SECRET_KEY"])
cluster = MongoClient("mongodb://binanceAdmin:binanceP%40ssword@159.65.3.230:27017/?authSource=admin")
db = cluster["BINANCE_DB"]
collection = db["SOCKET_COLLECTION"]
# post = {"portamount":"test","createdate":"test","updatedate":"test"}
# collection.insert_one(post)
info = client.get_account() # Getting account info
assets = []
values = []
for index in range(len(info['balances'])):
    for key in info['balances'][index]:
        if key == 'asset':
            assets.append(info['balances'][index][key])
        if key == 'free':
            values.append(info['balances'][index][key])


token_usdt = {}  # Dict to hold pair price in USDT
token_pairs = []  # List to hold different token pairs

# Creating token pairs and saving into a list
for token in assets:
    if token != 'USDT':
        token_pairs.append(token + 'USDT')


def streaming_data_process(msg):
    """
    Function to process the received messages and add latest token pair price
    into the token_usdt dictionary
    :param msg: input message
    """
    global token_usdt
    token_usdt[msg['s']] = msg['c']


def total_amount_usdt(assets, values, token_usdt):
    """
    Function to calculate total portfolio value in USDT
    :param assets: Assets list
    :param values: Assets quantity
    :param token_usdt: Token pair price dict
    :return: total value in USDT
    """

    total_amount = 0
    for i, token in enumerate(assets):
      try:
        if token != 'USDT':
            total_amount += float(values[i]) * float(
                token_usdt[token + 'USDT'])
        else:
            total_amount += float(values[i]) * 1
      except:
        pass

    # post = {"portamount":total_amount,"createdate":date_time,"updatedate":date_time}
    # collection.insert_one(post)
    return total_amount


def total_amount_btc(assets, values, token_usdt):
    """
    Function to calculate total portfolio value in BTC
    :param assets: Assets list
    :param values: Assets quantity
    :param token_usdt: Token pair price dict
    :return: total value in BTC
    """
    total_amount = 0
    for i, token in enumerate(assets):
      try:
        if token != 'BTC' and token != 'USDT':
            total_amount += float(values[i]) \
                            * float(token_usdt[token + 'USDT']) \
                            / float(token_usdt['BTCUSDT'])
        if token == 'BTC':
            total_amount += float(values[i]) * 1
        else:
            total_amount += float(values[i]) \
                            / float(token_usdt['BTCUSDT'])
      except:
        pass
    return total_amount


def assets_usdt(assets, values, token_usdt):
    """
    Function to convert all assets into equivalent USDT value
    :param assets: Assets list
    :param values: Assets quantity
    :param token_usdt: Token pair price dict
    :return: list of asset values in USDT
    """
    assets_in_usdt = []
    for i, token in enumerate(assets):
      try:
        if token != 'USDT':
            assets_in_usdt.append(
                float(values[i]) * float(token_usdt[token + 'USDT'])
            )
        else:
            assets_in_usdt.append(float(values[i]) * 1)
      except:
        pass
    return assets_in_usdt

bm = BinanceSocketManager(client)
for tokenpair in token_pairs:
    conn_key = bm.start_symbol_ticker_socket(tokenpair, streaming_data_process)
bm.start()

# content = []
#   bm = BinanceSocketManager(client)
#   for tokenpair in token_pairs:
#       conn_key = bm.start_symbol_ticker_socket(tokenpair, streaming_data_process)
#       print('test')
#       content.append(conn_key)
#   bm.start()
#   time.sleep(5)

# Streaming data for tokens in the portfolio

@app.route('/')
def index():
    return "Welcome to the scheduler!"

@app.route('/api/socket_price')
def socket_price():
    x = collection.find()
    return jsonify(x)

def scheduledTask():
    total_am = total_amount_usdt(assets, values, token_usdt)
    # time.sleep(1)
    now = datetime.now(tz=tz)
    date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
    post = {"portamount":total_am,"createdate":date_time,"updatedate":date_time}
    collection.insert_one(post)
    print("This task is running every 5 seconds"+date_time)


if __name__ == '__main__':
    scheduler.add_job(id ='Scheduled task', func = scheduledTask, trigger = 'interval', seconds = 6)
    scheduler.start()
    app.run(host = '127.0.0.1',port="8051",debug=True)
