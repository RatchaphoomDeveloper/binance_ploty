from flask import Flask,jsonify
import pymongo
from pymongo import MongoClient
from datetime import datetime,timezone,timedelta
from flask_cors import CORS
import time
app = Flask(__name__)
CORS(app)


cluster = MongoClient("mongodb://binanceAdmin:binanceP%40ssword@159.65.3.230:27017/?authSource=admin")
db = cluster["BINANCE_DB"]
collection = db["SOCKET_COLLECTION"]

@app.route('/api/socket_price')
def socket_price():
    results = collection.find({})
    dataArr = []
    for x in results:
      dt_obj = datetime.strptime(x['createdate'],
                           '%m/%d/%Y, %H:%M:%S')
      millisec = dt_obj.timestamp() * 1000
      dataArr.append([int(millisec), int(x['portamount'])])
    return jsonify(dataArr)

if __name__ == '__main__':
    app.run(host = '0.0.0.0',debug=True)
